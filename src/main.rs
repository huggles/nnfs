extern crate ndarray;
extern crate ndarray_rand;

mod nnfs;

use ndarray::{array, Array1, Array2};
use ndarray_rand::rand_distr::Uniform;
use ndarray_rand::RandomExt;

struct LayerDense {
    weights: Array2<f64>,
    bias: Array1<f64>,
}

impl LayerDense {
    pub fn new(n_inputs: usize, n_neurons: usize) -> Self {
        LayerDense {
            weights: 0.1 * Array2::random((n_inputs, n_neurons), Uniform::new(-1., 1.)),
            bias: Array1::zeros(n_neurons),
        }
    }

    pub fn forward(&self, inputs: Array2<f64>) -> Array2<f64> {
        inputs.dot(&self.weights) + &self.bias
    }
}

struct ActivationReLU {}

impl ActivationReLU {
    pub fn forward(&self, inputs: Array2<f64>) -> Array2<f64> {
        inputs.map(|i| i.to_owned().max(0.))
    }
}

fn main() {
    let data = nnfs::spiral_data(100, 3);

    let layer1 = LayerDense::new(2, 5);
    let activation1 = ActivationReLU {};

    println!("{}", activation1.forward(layer1.forward(data.0)));
}
